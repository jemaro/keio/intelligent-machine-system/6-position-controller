Andreu Gimenez Bolinches ([andreu@keio.jp](mailto:andreu@keio.jp))

# Problem

Develop a simulation program of position controller based on the disturbance
observer shown in slide 9-6 and evaluate the stability against the parameter
variation of $`J_m`$.

![9-6](images/9-6.jpg)

The problem has been solved using MATLAB Simulink. Here you can find an
image of the model used. One can find the source files in the [code
repository](https://gitlab.com/jemaro/keio/intelligent-machine-system/6-position-controller).

![model](images/model.png)

All terms of the disturbance torque are neglected except 
$`\Delta J_m \ddot{\theta}`$. 
The system has been evaluated against several variations of the
real parameter $`J_m`$ from its nominal counterpart $`J_{mn}`$ which stays
constant through all the tests. We have also evaluated the effect of the
parameter $`g`$ that controls the Disturbance Observer, being $`g = 0`$ the
scenario where no Disturbance Observer is used.

The following plots illustrate that when there is no disturbance, the
Disturbance Observer has a negligible effect on the performance of the system.

![1](images/1.png)


The following two groups of plots illustrate a scenario where the parameter
$`J_m`$ is a fraction of the modelled nominal value $`J_{mn}`$. When the nominal
$`J_{mn}`$ is more pessimistic than the real $`J_m`$, it can seem that the
disturbance observer is making the system to behave worse, but in reality it is
making it to behave as the previous plots where no disturbance was applied.
The disturbance observer attempts to make the system behave as it is modelled.

We can also start to see the effect of the parameter $`g`$. When if is not large
enough, the estimated disturbance is significantly smaller than the real one.
If it is increased, the estimated disturbance gets closer to the real one, but
lagging behind in time. If it is big enough, the estimated disturbance will be
very close to the real one.

![2](IMAGES/2.png)

![3](images/3.png)


The following group of plots show a scenario when the parameter $`J_m`$ is
doubled from its modelled nominal counterpart $`J_{mn}`$. In this case, the
modelled nominal value $`J_{mn}`$ is optimistic in comparison to the real value
$`J_m`$ and the Disturbance Observer improves significantly the performance of the
system.

![4](images/4.png)


The following two groups of plots illustrate the scenario when the parameter
$`J_m`$ is several times bigger than its nominal value $`J_{mn}`$. Here we can see
that the parameter $`g`$ needs to be tuned properly otherwise the
system can destabilize.

![5](images/5.png)

![6](images/6.png)

To sum up, a carefully tuned Disturbance Observer can make a system robust
against modelling errors. However, this study has not taken into account the
trade-off of the Disturbance Observer parameter $`g`$, as the velocity
measurements are noiseless.

# Recent development of robust control with machine learning

In this section, three recently highlighted research papers are dissected in
a few words and some general conclusion about the field is presented at the
end.

## Donti et al., 2021: [Enforcing robust control guarantees within neural network policies](https://arxiv.org/abs/2011.08105)

### What is the method/technique? 

A class of nonlinear control policies, potentially parameterized by deep
neural networks, that guarantee to obey the same stability conditions
enforced by the robustness specifications of traditional robust control.

### For which application? 

High performance robust control. Traditional simple (often linearized) policies
have guarantees of robust control under worst-case conditions, while
Reinforcement Learning based control has very high performance even in highly
complex nonlinear systems at the cost of being often unstable in presence of
system disturbances.

### Why is it working? How is validated? 

Simulated in state-of-the-art control tasks like the cart-pole task with
different robust Reinforced Learning control policies and compared to their
non-robust counterparts, traditional robust controllers and other robust
Reinforced Learning approaches that are not _provable_.

Additionally, these simulations are performed with two different dynamics
settings. What could be called original dynamics or "average case" and
_Adversarial dynamics_ with a disturbance generated to maximize loss in
training time.

### What is missing? How could it be improved? 

It's not mentioned the applicability of this method in a real environment,
whether it is able to control one of the benchmark tasks in real time and which
hardware is needed for that.

## Ho et al., 2021: [Online Robust Control of Nonlinear Systems with Large Uncertainty](http://proceedings.mlr.press/v130/ho21a.html)

### What is the method/technique? 

An online decision rule that guarantees to control any system in a finite time
given only a rough model of it (_online control with mistake guarantees_).

### For which application? 

Nonlinear systems with large uncertainty, for example real-world robotic
systems like robot manipulators.

### Why is it working? How is validated? 

Simulated on a cart-pole system with a swing-up goal with additional real-word
constraints and high fidelity dynamic models.

### What is missing? How could it be improved?

Broader benchmarking, even if it is stated as the first work of online robust
control of nonlinear systems with large uncertainty, it would be useful to see
the results compared to current methods. These other methods will have a
significant advantage as they require more a-priori information of the system
but given the effort put in simulating real world scenarios, it would be useful
to compare it to what is being used in the real world right now.

## Okano et al., 2020 [Development of Neural Network-based Explicit Force Control with Disturbance Observer](https://www.jstage.jst.go.jp/article/ieejjia/10/2/10_20004711/_pdf/-char/en)

### What is the method/technique? 

Adding a Neural Network component into a force controller that already uses a
Disturbance Observer and a Reaction Force Observer.

### For which application? 

Devices where force controllers provide a performance enhancement but the
environmental impedance and model uncertainties are big enough to make it
difficult to design a force controller and select the appropriate gains. Like
human support robots.

### Why is it working? How is validated?

Both simulations and experiments. Training is performed initially in simulation
but also in the actual experimental setup. The simulated and experimented
situations is quite broad, with edge cases like faulty network training.

### What is missing? How could it be improved?

The study selects a Neural Network structure and composition through
experimental validation at training time and concludes that the proposed method
increases the performance over the conventional method. But I'm curious on how
the NN affects the computational complexity of the overall system and how
simpler or more complex NN affect the performance at test time as well.

## Conclusion

Robust control guarantees are highly desirable in a real-world environment.
Neural Networks and other Machine Learning techniques are a trendy keyword in
the engineering industry and their success in different fields push other
fields to try to implement them. They usually provide an increment of
performance in design scenarios but it is uncertain how they perform in edge
cases.

The papers summarized above implement Machine Learning in the field of control
without renouncing to the robust control guarantees in three different ways:
Modifying Neural Network policies to guarantee robustness; Online robust
control instead of system identification prior to controller design; Using both
conventional robust control techniques, like a Disturbance Observer, and a
Neural Network.

Machine Learning research gets significant attention and evolves fast, we are
sure to see even more different approaches to robust control in the coming
years.
