{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Andreu Gimenez Bolinches ([andreu@keio.jp](mailto:andreu@keio.jp))\n",
    "\n",
    "# Problem\n",
    "\n",
    "Develop a simulation program of position controller based on the disturbance\n",
    "observer shown in slide 9-6 and evaluate the stability against the parameter\n",
    "variation of $J_m$.\n",
    "\n",
    "![9-6](images/9-6.jpg)\n",
    "\n",
    "The problem has been solved using MATLAB Simulink. Here you can find an\n",
    "image of the model used. One can find the source files in the [code\n",
    "repository](https://gitlab.com/jemaro/keio/intelligent-machine-system/6-position-controller).\n",
    "\n",
    "![model](images/model.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All terms of the disturbance torque are neglected except \n",
    "$\\Delta J_m \\ddot{\\theta}$. \n",
    "The system has been evaluated against several variations of the\n",
    "real parameter $J_m$ from its nominal counterpart $J_{mn}$ which stays\n",
    "constant through all the tests. We have also evaluated the effect of the\n",
    "parameter $g$ that controls the Disturbance Observer, being $g = 0$ the\n",
    "scenario where no Disturbance Observer is used.\n",
    "\n",
    "The following plots illustrate that when there is no disturbance, the\n",
    "Disturbance Observer has a negligible effect on the performance of the system.\n",
    "\n",
    "![1](images/1.png)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following two groups of plots illustrate a scenario where the parameter\n",
    "$J_m$ is a fraction of the modelled nominal value $J_{mn}$. When the nominal\n",
    "$J_{mn}$ is more pessimistic than the real $J_m$, it can seem that the\n",
    "disturbance observer is making the system to behave worse, but in reality it is\n",
    "making it to behave as the previous plots where no disturbance was applied.\n",
    "The disturbance observer attempts to make the system behave as it is modelled.\n",
    "\n",
    "We can also start to see the effect of the parameter $g$. When if is not large\n",
    "enough, the estimated disturbance is significantly smaller than the real one.\n",
    "If it is increased, the estimated disturbance gets closer to the real one, but\n",
    "lagging behind in time. If it is big enough, the estimated disturbance will be\n",
    "very close to the real one.\n",
    "\n",
    "![2](IMAGES/2.png)\n",
    "\n",
    "![3](images/3.png)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following group of plots show a scenario when the parameter $J_m$ is\n",
    "doubled from its modelled nominal counterpart $J_{mn}$. In this case, the\n",
    "modelled nominal value $J_{mn}$ is optimistic in comparison to the real value\n",
    "$J_m$ and the Disturbance Observer improves significantly the performance of the\n",
    "system.\n",
    "\n",
    "![4](images/4.png)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following two groups of plots illustrate the scenario when the parameter\n",
    "$J_m$ is several times bigger than its nominal value $J_{mn}$. Here we can see\n",
    "that the parameter $g$ needs to be tuned properly otherwise the\n",
    "system can destabilize.\n",
    "\n",
    "![5](images/5.png)\n",
    "\n",
    "![6](images/6.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To sum up, a carefully tuned Disturbance Observer can make a system robust\n",
    "against modelling errors. However, this study has not taken into account the\n",
    "trade-off of the Disturbance Observer parameter $g$, as the velocity\n",
    "measurements are noiseless."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Recent development of robust control with machine learning\n",
    "\n",
    "In this section, three recently highlighted research papers are dissected in\n",
    "a few words and some general conclusion about the field is presented at the\n",
    "end."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Donti et al., 2021: [Enforcing robust control guarantees within neural network policies](https://arxiv.org/abs/2011.08105)\n",
    "\n",
    "### What is the method/technique? \n",
    "\n",
    "A class of nonlinear control policies, potentially parameterized by deep\n",
    "neural networks, that guarantee to obey the same stability conditions\n",
    "enforced by the robustness specifications of traditional robust control.\n",
    "\n",
    "### For which application? \n",
    "\n",
    "High performance robust control. Traditional simple (often linearized) policies\n",
    "have guarantees of robust control under worst-case conditions, while\n",
    "Reinforcement Learning based control has very high performance even in highly\n",
    "complex nonlinear systems at the cost of being often unstable in presence of\n",
    "system disturbances.\n",
    "\n",
    "### Why is it working? How is validated? \n",
    "\n",
    "Simulated in state-of-the-art control tasks like the cart-pole task with\n",
    "different robust Reinforced Learning control policies and compared to their\n",
    "non-robust counterparts, traditional robust controllers and other robust\n",
    "Reinforced Learning approaches that are not _provable_.\n",
    "\n",
    "Additionally, these simulations are performed with two different dynamics\n",
    "settings. What could be called original dynamics or \"average case\" and\n",
    "_Adversarial dynamics_ with a disturbance generated to maximize loss in\n",
    "training time.\n",
    "\n",
    "### What is missing? How could it be improved? \n",
    "\n",
    "It's not mentioned the applicability of this method in a real environment,\n",
    "whether it is able to control one of the benchmark tasks in real time and which\n",
    "hardware is needed for that."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ho et al., 2021: [Online Robust Control of Nonlinear Systems with Large Uncertainty](http://proceedings.mlr.press/v130/ho21a.html)\n",
    "\n",
    "### What is the method/technique? \n",
    "\n",
    "An online decision rule that guarantees to control any system in a finite time\n",
    "given only a rough model of it (_online control with mistake guarantees_).\n",
    "\n",
    "### For which application? \n",
    "\n",
    "Nonlinear systems with large uncertainty, for example real-world robotic\n",
    "systems like robot manipulators.\n",
    "\n",
    "### Why is it working? How is validated? \n",
    "\n",
    "Simulated on a cart-pole system with a swing-up goal with additional real-word\n",
    "constraints and high fidelity dynamic models.\n",
    "\n",
    "### What is missing? How could it be improved?\n",
    "\n",
    "Broader benchmarking, even if it is stated as the first work of online robust\n",
    "control of nonlinear systems with large uncertainty, it would be useful to see\n",
    "the results compared to current methods. These other methods will have a\n",
    "significant advantage as they require more a-priori information of the system\n",
    "but given the effort put in simulating real world scenarios, it would be useful\n",
    "to compare it to what is being used in the real world right now."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Okano et al., 2020 [Development of Neural Network-based Explicit Force Control with Disturbance Observer](https://www.jstage.jst.go.jp/article/ieejjia/10/2/10_20004711/_pdf/-char/en)\n",
    "\n",
    "### What is the method/technique? \n",
    "\n",
    "Adding a Neural Network component into a force controller that already uses a\n",
    "Disturbance Observer and a Reaction Force Observer.\n",
    "\n",
    "### For which application? \n",
    "\n",
    "Devices where force controllers provide a performance enhancement but the\n",
    "environmental impedance and model uncertainties are big enough to make it\n",
    "difficult to design a force controller and select the appropriate gains. Like\n",
    "human support robots.\n",
    "\n",
    "### Why is it working? How is validated?\n",
    "\n",
    "Both simulations and experiments. Training is performed initially in simulation\n",
    "but also in the actual experimental setup. The simulated and experimented\n",
    "situations is quite broad, with edge cases like faulty network training.\n",
    "\n",
    "### What is missing? How could it be improved?\n",
    "\n",
    "The study selects a Neural Network structure and composition through\n",
    "experimental validation at training time and concludes that the proposed method\n",
    "increases the performance over the conventional method. But I'm curious on how\n",
    "the NN affects the computational complexity of the overall system and how\n",
    "simpler or more complex NN affect the performance at test time as well."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "Robust control guarantees are highly desirable in a real-world environment.\n",
    "Neural Networks and other Machine Learning techniques are a trendy keyword in\n",
    "the engineering industry and their success in different fields push other\n",
    "fields to try to implement them. They usually provide an increment of\n",
    "performance in design scenarios but it is uncertain how they perform in edge\n",
    "cases.\n",
    "\n",
    "The papers summarized above implement Machine Learning in the field of control\n",
    "without renouncing to the robust control guarantees in three different ways:\n",
    "Modifying Neural Network policies to guarantee robustness; Online robust\n",
    "control instead of system identification prior to controller design; Using both\n",
    "conventional robust control techniques, like a Disturbance Observer, and a\n",
    "Neural Network.\n",
    "\n",
    "Machine Learning research gets significant attention and evolves fast, we are\n",
    "sure to see even more different approaches to robust control in the coming\n",
    "years."
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "bee6743b0ec78d9381c421ea90ba84a9da048a66a587d4ab52314ae4c0fd816c"
  },
  "kernelspec": {
   "display_name": "Python 3.8.4 64-bit ('venv': venv)",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.4"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
