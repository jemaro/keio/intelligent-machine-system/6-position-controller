initModelData;

f = figure(1);
Jm_list = [1, 0.25, 0.5, 2, 4, 8];

for k = 1:length(Jm_list)
    Jm_perc = Jm_list(k);
    Jm = Jm_perc * Jmn;

    % Plots setup
    ax_pos = subplot(1, 2, 1, 'replace');
    title(['Position J_m = ' num2str(Jm_perc) ' J_{mn}']);
    ylabel('\theta [rad]');
    xlabel('Time [s]');
    hold on;
    ax_dis = subplot(1, 2, 2, 'replace');
    title(['Disturbance J_m = ' num2str(Jm_perc) ' J_{mn}']);
    ylabel('T_{dis}');
    xlabel('Time [s]');
    hold on;

    g_list = [0, 1, 10, 100];
    colors = {'#0072BD', '#D95319', '#7E2F8E', '#77AC30'};

    for i = 1:length(g_list)
        g = g_list(i);
        [q, q_ref, ~, Tdis, Tdis_est] = runModel();

        if i == 1
            plot(q_ref.Values, 'k--', 'Parent', ax_pos, ...
                'DisplayName', 'Reference');
        end

        plot(q.Values, '-', 'Color', colors{i}, 'Parent', ax_pos, ...
            'DisplayName', ['g = ', num2str(g)]);

        plot(Tdis_est.Values, '-', 'Color', colors{i}, 'Parent', ax_dis, ...
            'DisplayName', ['Estimated g = ', num2str(g)]);
        plot(Tdis.Values, '--', 'Color', colors{i}, 'Parent', ax_dis, ...
            'DisplayName', ['Real g = ', num2str(g)]);
    end

    hold(ax_pos, 'off');
    hold(ax_dis, 'off');

    legend(ax_pos, 'Location', 'SouthEast');
    legend(ax_dis, 'Location', 'NorthEast');

    saveas(f, [pwd '/images/' num2str(k) '.png'])

end


function [q, q_ref, v, Tdis, Tdis_est] = runModel()
    simout = sim('model');
    q = get(simout.yout, 'position');
    q_ref = get(simout.yout, 'position_ref');
    v = get(simout.yout, 'velocity');
    Tdis = get(simout.yout, 'disturbance');
    Tdis_est = get(simout.yout, 'disturbance_est');
end
