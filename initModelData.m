% Control gains
Kp = 100;
Kv = 10;
% Nominal motor parameters
Jmn = 1;
Ktn = 1;

% Disturbance
if ~exist('Jm', 'var')
    Jm = Jmn;
end

% Disturbance Observer
if ~exist('g', 'var')
    g = 0;
end
